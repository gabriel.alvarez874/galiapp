pipeline {
  agent any

environment {
    registry = "GaliApp"
}

  stages {
	stage('SCM') {
        steps{
            echo """#
             REALIZANDO SCM
            """
            git branch: 'main', credentialsId: '002', url: 'git@gitlab.com:gabriel.alvarez874/galiapp.git'
        }//steps
	}//stage
	
    stage('Build') {
        steps{
            echo """#
             REALIZANDO COMPILACION 
            """
            script {
            docker.build registry + ":$BUILD_NUMBER"
            }
        }//steps
	}//stage
  }//stages
}//pipeline